#! /usr/bin/env python

import json
from pathlib import Path
import numpy as np
from keras import models
import subprocess
from MultibandImage import load_paths
from MultibandImage import MultibandImage

idx = "10010"
path_to_images = Path("../get_images/random")
image_name = path_to_images / f"image_{idx}_r.fits"

encoder = models.load_model("encoder.h5")

image = MultibandImage([image_name], "r")
image.get_obj_params()
image.crop_all_data(shift_variance=0)
res = image.zoom_all_data(target_size=64)
if res is False:
    # Cropped image is too small
    print("too small")
    exit()
image.norm_all_data()
print(image.as_array().shape)
new_data = np.reshape(image.as_array(), (64, 64, 1))
target_code = encoder.predict(new_data[None])[0][:-3]

codes = json.load(open("codes.dat"))

names = []
dists = []
for fname, code in codes.items():
    dist = np.sum((target_code - np.array(code)) ** 2)
    if dist < 0.5:
        names.append(fname)
        dists.append(dist)

inds = np.argsort(dists)

path_to_random = Path("../get_images/random")
for idx in inds[1:11]:
    print(idx)
    call_str = f"ds9 {path_to_random / Path(names[idx]).name} -scale log"
    subprocess.call(call_str, shell=True)
    print(dists[idx], path_to_random / Path(names[idx]).name)
