#! /usr/bin/env python

from pathlib import Path
import json
import numpy as np
from keras import models
from MultibandImage import load_paths
from MultibandImage import MultibandImage


def prepare(path_list):
    image = MultibandImage(path_list, "r")
    image.rotate_all_data()
    image.get_obj_params()
    image.add_noise_to_all_data(noise_frac=0)
    image.crop_all_data(shift_variance=0.25)
    res = image.zoom_all_data(target_size=64)
    if res is False:
        # Cropped image is too small
        return None
    image.norm_all_data()
    new_data = image.as_array()
    if len(np.where(np.isnan(new_data))[0]) > 1:
        return None
    return new_data


random_images = load_paths(Path("../prepare_images/random/"), 'r')
encoder = models.load_model("encoder.h5")

codes = {}
for img in random_images:
    print(img)
    image = MultibandImage(img, "r")
    image.get_obj_params()
    image.crop_all_data(shift_variance=0)
    res = image.zoom_all_data(target_size=64)
    if res is False:
        # Cropped image is too small
        continue
    image.norm_all_data()
    new_data = np.reshape(image.as_array(), (64, 64, 1))
    if len(np.where(np.isnan(new_data))[0]) > 1:
        continue
    codes[str(img[0])] = [float(v) for v in encoder.predict(new_data[None])[0][:-3]]

print(codes)
with open("codes.dat", "w") as fout:
    json.dump(codes, fout)
