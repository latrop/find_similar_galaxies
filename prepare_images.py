#!/usr/bin/env python

from pathlib import Path
import random
import argparse
import warnings
import multiprocessing
import numpy as np
from scipy.ndimage import rotate
from MultibandImage import load_paths
from MultibandImage import MultibandImage
warnings.filterwarnings('ignore', '.*output shape of zoom.*')
warnings.filterwarnings('ignore', '.*were found*')


def prepare(path_list):
    image1 = MultibandImage(path_list, "r")
    image1.rotate_all_data()
    image1.get_obj_params()
    image1.mask_all_data()
    image1.add_noise_to_all_data(noise_frac=0.2)
    image1.crop_all_data(shift_variance=0.1)
    res = image1.zoom_all_data(target_size=args.size)
    if res is False:
        # Cropped image is too small
        return None
    image1.norm_all_data()
    new_data1 = image1.as_array()[0]
    if len(np.where(np.isnan(new_data1))[0]) > 1:
        return None

    image2 = MultibandImage(path_list, "r")
    image2.rotate_all_data()
    image2.get_obj_params()
    image2.mask_all_data()
    image2.add_noise_to_all_data(noise_frac=0.2)
    image2.crop_all_data(shift_variance=0.1)
    res = image2.zoom_all_data(target_size=args.size)
    if res is False:
        # Cropped image is too small
        return None
    image2.norm_all_data()
    new_data2 = image2.as_array()[0]
    if len(np.where(np.isnan(new_data2))[0]) > 1:
        return None
    
    res = np.dstack((new_data1, new_data2, np.zeros_like(new_data1)))
    return res


def main(args):
    random_images = load_paths(Path("/mnt/ramdisk/random"), args.bands)
    chunk_size = 24 * 400
    number_of_prepared_images = 0
    out_data = None
    while number_of_prepared_images < args.num:
        chunk_data = []
        pool = multiprocessing.Pool(24)
        for i in range(chunk_size):
            chunk_data.append(random.choice(random_images))
        chunk_results = pool.map(prepare, chunk_data)
        print("Chunk done")
        chunk_good_data = []
        for i in range(chunk_size):
            if chunk_results[i] is not None:
                chunk_good_data.append(chunk_results[i])
                number_of_prepared_images += 1
        print(number_of_prepared_images)
        if out_data is None:
            out_data = np.array(chunk_good_data)
        else:
            out_data = np.append(out_data, chunk_good_data, axis=0)

    np.save(file=f"data_{args.idx}.npy", arr=out_data)


if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--size", help="Size of the resulting images", type=int, default=64)
    argument_parser.add_argument("--num", help="Number of images to make", type=int, default=10)
    argument_parser.add_argument("--bands", help="List of bands. The first one is used as reference.")
    argument_parser.add_argument("--idx", help="Index to attach to the end of the files.", type=int)
    args = argument_parser.parse_args()

    main(args)
