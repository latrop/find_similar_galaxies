#! /usr/bin/env python

from keras.layers import Dense, Flatten, Reshape, Input, InputLayer, Conv2D, MaxPooling2D, UpSampling2D, Dropout
from keras.models import Sequential, Model
import numpy as np
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm


def build_autoencoder():
    encoder = Sequential()
    encoder.add(InputLayer((64, 64, 1)))
    encoder.add(Conv2D(32, kernel_size=5, activation='relu', padding='same', input_shape=(64, 64, 1)))
    encoder.add(Conv2D(32, kernel_size=5, activation='relu', padding='same'))
    encoder.add(MaxPooling2D(pool_size=(2, 2), strides=None, padding='valid', data_format=None))

    encoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
    encoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
    encoder.add(MaxPooling2D(pool_size=(2, 2), strides=None, padding='valid', data_format=None))
    
    encoder.add(Conv2D(8, kernel_size=3, activation='relu', padding='same'))
    encoder.add(Conv2D(8, kernel_size=3, activation='relu', padding='same'))

    encoder.add(Flatten())
    encoder.add(Dense(40))

    decoder = Sequential()
    decoder.add(InputLayer((40, )))
    decoder.add(Dense(8*8*8))
    decoder.add(Reshape((8, 8, 8)))

    decoder.add(Conv2D(8, kernel_size=3, activation='relu', padding='same'))
    decoder.add(Conv2D(8, kernel_size=3, activation='relu', padding='same'))
    decoder.add(UpSampling2D((2, 2)))

    decoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
    decoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
    decoder.add(UpSampling2D((2, 2)))

    decoder.add(Conv2D(32, kernel_size=5, activation='relu', padding='same'))
    decoder.add(Conv2D(32, kernel_size=5, activation='relu', padding='same'))
    decoder.add(UpSampling2D((2, 2)))
    
    decoder.add(Conv2D(1, kernel_size=3, activation='relu', padding='same'))
    
    return encoder, decoder


def visualize(img, encoder, decoder, outname):
    """Draws original, encoded and decoded images"""
    #img = img.reshape((-1, 64, 64, 1))
    code = encoder.predict(img[None])[0]
    reco = decoder.predict(code[None])[0]

    vmin = max(0.01, np.percentile(data, 1))
    vmax = np.percentile(data, 99)
    
    plt.subplot(1,3,1)
    plt.title("Original")
    plt.imshow(img, vmin=vmin, vmax=vmax, cmap="gray")

    plt.subplot(1,3,2)
    plt.title("Code")
    plt.imshow(code.reshape([code.shape[-1]//2,-1]))

    plt.subplot(1,3,3)
    plt.title("Reconstructed")
    plt.imshow(reco, vmin=vmin, vmax=vmax, cmap="gray")
    plt.savefig(outname)
    plt.clf()


def train(x_train, x_test):
    encoder, decoder = build_autoencoder()
    inp = Input((64, 64, 1))
    code = encoder(inp)
    reconstruction = decoder(code)
    autoencoder = Model(inp, reconstruction)
    autoencoder.compile(optimizer='adamax', loss='mse')
    history = autoencoder.fit(x=x_train, y=x_train, epochs=25,
                              validation_data=[x_test, x_test])
    return encoder, decoder


data = np.load("train.npy")
x_train, x_test = train_test_split(data, test_size=0.25)
x_train = x_train.reshape(-1, 64, 64, 1)
x_test = x_test.reshape(-1, 64, 64, 1)

encoder, decoder = train(x_train, x_test)
encoder.save("encoder.h5")
decoder.save("decoder.h5")
for idx in range(200):
    visualize(x_test[idx], encoder, decoder, outname=f"reco/reco_{idx}.png")
